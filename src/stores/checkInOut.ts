import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type CheckInOut from "../types/CheckInOut";
import checkInOut from "../services/checkInOut";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
export const useCheckInOut = defineStore("Cio", () => {
  const email = localStorage.getItem("email");
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const btn = ref(false);
  const cio = ref<CheckInOut[]>([]);
  async function getCio() {
    loadingStore.isLoading = true;
    try {
      const res = await checkInOut.getCio();
      cio.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล CheckInOut ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCioByEmail(email: string) {
    loadingStore.isLoading = true;
    try {
      const res = await checkInOut.getCioByEmail(email);
      cio.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล CheckInOut ได้");
    }
    loadingStore.isLoading = false;
  }

  async function check(email: string) {
    try {
      const cioo = await checkInOut.check(email);
      btn.value = cioo.data;
      console.log(btn.value);
      console.log(email);
      console.log(cioo);
    } catch (e) {
      console.log(e);
    }
  }

  async function saveCio() {
    loadingStore.isLoading = true;
    const js = { email: email! };
    try {
      const ress = await checkInOut.saveCio(js);
    } catch (e) {
      console.log(e);
    }
    await check(email!);
    await getCioByEmail(email!);
    loadingStore.isLoading = false;
  }

  async function updateCio() {
    loadingStore.isLoading = true;
    const js = { email: email! };
    try {
      const ress = await checkInOut.updateCio(js);
      console.log(ress);
    } catch (e) {
      console.log(e);
    }
    await check(email!);
    await getCioByEmail(email!);
    loadingStore.isLoading = false;
  }

  return { cio, getCio, saveCio, check, btn, updateCio, getCioByEmail };
});
