export default interface Product {
  id?: number;
  name: string;
  price: number;
  type?: string;
  size?: string;
  image?: string;
  categoryId: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
