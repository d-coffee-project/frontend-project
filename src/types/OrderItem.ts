import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  id?: number;
  productId?: number;
  product?: Product;

  name?: string;
  price?: number;
  amount: number;
  sum?: number;
  total?: number;
  order?: Order;

  createDate?: Date;
  updateDate?: Date;
  deletedDate?: Date;
}
