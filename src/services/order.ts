import type Order from "@/types/Order";
import type OrderItem from "@/types/OrderItem";
import http from "./axios";
function getOrder() {
  return http.get("/orders");
}

function getOrderID(id: number) {
  return http.get(`/orders/${id}`);
}
function saveOrder(Order: {
  orderItems: { productId: number; amount: number }[];
  userId: number;
}) {
  return http.post("/orders", Order);
}
function updateOrder(id: number, Order: Order) {
  return http.patch(`/orders/${id}`, Order);
}
function deleteOrder(item: OrderItem) {
  return http.delete(`/orders/${item}`);
}

function getHisByName(id: number) {
  return http.get(`/orders/order/${id}`);
}

export default {
  getOrder,
  saveOrder,
  updateOrder,
  deleteOrder,
  getHisByName,
  getOrderID,
};
