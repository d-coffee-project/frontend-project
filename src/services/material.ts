import type Material from "@/types/Material";
import http from "./axios";
function getMaterials() {
  return http.get("/report/material");
}

function getMaterialsById() {
  return http.get("/materials/:id");
}
function saveMaterial(material: Material) {
  return http.post("/materials", material);
}

function updateMaterial(mat_id: number, material: Material) {
  return http.patch(`/materials/${mat_id}`, material);
}

function deleteMaterial(mat_id: number) {
  return http.delete(`/materials/${mat_id}`);
}

function getMaterialsByName(name: string) {
  return http.get(`/materials/material/${name}`);
}

export default {
  getMaterials,
  saveMaterial,
  updateMaterial,
  deleteMaterial,
  getMaterialsById,
  getMaterialsByName,
};
