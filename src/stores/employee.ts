import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useEmployeeStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const userId = ref();
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee & { files: File[] }>({
    name: "",
    tel: "",
    email: "",
    address: "",
    position: "",
    wage: 0,
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        tel: "",
        email: "",
        address: "",
        position: "",
        wage: 0,
        files: [],
      };
    }
  });

  async function getEmployee_info() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployee_info();
      employees.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getEmployee() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployee();
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      console.log("editedEmployee:: ", editedEmployee.value.files);
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;
      await getEmployee();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmployee();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }

  async function searchEMP(event: Event) {
    const { target } = event;
    const key = (target as HTMLButtonElement).value;
    // loadingStore.isLoading = true;
    try {
      let data = [];
      if (key) {
        const res = await employeeService.getEmployeeByName(key);
        data = res.data;
      } else {
        const res = await employeeService.getEmployee();
        data = res.data;
      }
      employees.value = data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
    // loadingStore.isLoading = false;
  }

  return {
    employees,
    getEmployee,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
    getEmployee_info,
    searchEMP,
  };
});
