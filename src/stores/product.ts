import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import category from "@/services/category";

export const useProductStore = defineStore("Product", () => {
  const dialogAdd = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const category = ref(1);
  const editedProduct = ref<Product & { files: File[] }>({
    name: "",
    price: 0,
    categoryId: 1,
    image: "search-empty.png",
    files: [],
    type: "",
    size: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = {
        name: "",
        price: 0,
        categoryId: 1,
        image: "search-empty.png",
        files: [],
        type: "",
        size: "",
      };
    }
  });
  watch(category, async (newCategory, oldCategory) => {
    await getProductsByCategory(newCategory);
  });
  async function getProductsByCategory(category: number) {
    //เป็นการ get product จากตัวserver
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProductsByCategory(category);
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }
  async function getProducts() {
    //เป็นการ get product จากตัวserver
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Product ได้");
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product)); //แปลงให้เป็น string และแปลงให้กลับมาเป็น Objectอีกครั้ง
    dialog.value = true;
  }

  async function searchProduct(event: Event, category: number) {
    const { target } = event;
    const key = (target as HTMLButtonElement).value;
    console.log("key:: ", category);
    loadingStore.isLoading = true;
    try {
      let data = [];
      if (key && category) {
        const res = await productService.getProductsByCategoryAndName(
          category,
          key
        );
        data = res.data;
      } else if (!key && category) {
        const res = await productService.getProductsByCategory(category);
        data = res.data;
      } else {
        const res = await productService.getProducts();
        data = res.data;
      }
      products.value = data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  function editAddProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product)); //แปลงให้เป็น string และแปลงให้กลับมาเป็น Objectอีกครั้ง
    dialogAdd.value = true;
  }

  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    category,
    getProductsByCategory,
    searchProduct,
    editAddProduct,
    dialogAdd,
  };
});
