import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomers() {
  return http.get("/customers");
}

function getCustomers_info() {
  return http.get("/report/customer");
}

function saveCustomer(customer: Customer) {
  return http.post("/customers", customer);
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customers/${id}`, customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

// const findCustomer=(tel:string)=>{
//   return http.get(`/customer/`)
// }

function getUserByName(name: string) {
  return http.get(`/customers/customer/${name}`);
}

export default {
  getCustomers,
  saveCustomer,
  updateCustomer,
  deleteCustomer,
  getUserByName,
  getCustomers_info,
};
