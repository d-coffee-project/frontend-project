import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useReportStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee & { files: File[] }>({
    name: "",
    tel: "",
    email: "",
    address: "",
    position: "",
    wage: 0,
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        tel: "",
        email: "",
        address: "",
        position: "",
        wage: 0,
        files: [],
      };
    }
  });

  async function getEmployee_info() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployee_info();
      employees.value = res.data[0];
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    getEmployee_info,
  };
});
