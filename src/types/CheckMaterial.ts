import type CheckMaterialDetail from "./CheckMaterialDetail";
import type Employee from "./Employee";

export default interface CheckMaterial {
  check_mat_id?: number;
  empId?: number;
  employee?: Employee;
  check_mat_date?: string;
  check_mat_time?: string;
  check_material_detail?: CheckMaterialDetail[];
}
