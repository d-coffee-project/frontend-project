import type Employee from "./Employee";

export default interface CheckInOut {
  id?: number;

  date?: string;

  email?: string;

  time_in?: string;

  time_out?: string;

  total_hour?: number;

  employee?: Employee;
}
