import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Salary from "@/types/Salary";
import salaryService from "@/services/salary";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useSalaryStore = defineStore("Salary", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const salarys = ref<Salary[]>([]);
  const editedSalary = ref<Salary>({
    date: "",
    workhour: 0,
    salary: 0,
    status: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedSalary.value = { date: "", workhour: 0, salary: 0, status: "" };
    }
  });

  async function getSalarys() {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.getSalary();
      salarys.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Salary ได้");
    }
    loadingStore.isLoading = false;
  }
  async function getSalaryBySt(st: string) {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.getSalaryBySt(st);
      salarys.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Salary ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveSalary() {
    loadingStore.isLoading = true;
    try {
      if (editedSalary.value.id) {
        const res = await salaryService.editSalary(
          editedSalary.value.id,
          editedSalary.value
        );
      } else {
        const res = await salaryService.saveSalary(editedSalary.value);
      }

      dialog.value = false;
      await getSalarys();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Salary ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteSalary(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.deleteSalary(id);
      await getSalarys();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Salary ได้");
    }
    loadingStore.isLoading = false;
  }

  function editSalary(salary: Salary) {
    editedSalary.value = JSON.parse(JSON.stringify(salary));
    dialog.value = true;
  }

  async function updateSalary(salary: Salary) {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.updateSalary(salary.id || 0);
      await getSalarys();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Salary ได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    salarys,
    getSalarys,
    dialog,
    editedSalary,
    saveSalary,
    editSalary,
    updateSalary,
    deleteSalary,
    getSalaryBySt,
  };
});
