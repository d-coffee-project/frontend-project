import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
// import customer from "@/services/customer";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const discount = ref(0);
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({ name: "", tel: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: "" };
    }
  });

  // async function getCustomers_info() {
  //   loadingStore.isLoading = true;
  //   try {
  //     const res = await customerService.getCustomers_info();
  //     customers.value = res.data[0];
  //   } catch (e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
  //   }
  //   loadingStore.isLoading = false;
  // }

  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomers();
      customers.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.deleteCustomer(id);
      await getCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }

  const findCustomer = (tel: string) => {
    const find = customers.value.find((item) => item.tel === tel);
    if (find) {
      discount.value = 5;
      return find;
    }
    discount.value = 0;
    return { name: "Non member", tel };
  };

  async function searchCus(event: Event) {
    const { target } = event;
    const key = (target as HTMLButtonElement).value;
    // loadingStore.isLoading = true;
    try {
      let data = [];
      if (key) {
        const res = await customerService.getUserByName(key);
        data = res.data;
      } else {
        const res = await customerService.getCustomers();
        data = res.data;
      }
      customers.value = data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
    // loadingStore.isLoading = false;
  }

  return {
    customers,
    getCustomers,
    dialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    deleteCustomer,
    findCustomer,
    discount,
    searchCus,
  };
});
