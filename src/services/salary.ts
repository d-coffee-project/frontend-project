import type Salary from "@/types/Salary";
import http from "./axios";
function getSalary() {
  return http.get("/summary-salary");
}

function saveSalary(summary: Salary) {
  return http.post("/summary-salary", summary);
}

function editSalary(id: number, summary: Salary) {
  return http.patch(`/summary-salary/${id}`);
}

function updateSalary(id: number) {
  return http.patch(`/summary-salary/${id}`);
}

function deleteSalary(id: number) {
  return http.delete(`/summary-salary/${id}`);
}

function getSalaryByName(name: string) {
  return http.get(`/summary-salary/summary-salary/${name}`);
}

function getSalaryBySt(st: string) {
  return http.get(`/summary-salary/${st}`);
}

export default {
  getSalary,
  saveSalary,
  deleteSalary,
  updateSalary,
  getSalaryByName,
  editSalary,
  getSalaryBySt,
};
