export default interface BillDetail {
  materialId?: number;
  name?: string;
  amount?: number;
  price?: number;
  total?: number;

  createDate?: Date;
  updateDate?: Date;
  deletedDate?: Date;
}
