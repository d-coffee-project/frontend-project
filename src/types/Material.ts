export default interface Material {
  mat_id?: number;
  mat_name: string;
  mat_min_quantity: number;
  mat_quantity: number;
  mat_unit: string;
  mat_price_per_unit: number;
}
