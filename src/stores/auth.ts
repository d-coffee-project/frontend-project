import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authName = ref("");

  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString ?? "");
    return user;
  };

  const isLogin = () => {
    return !!localStorage.getItem("token"); // ตรวจสอบว่ามี token หรือไม่
  };

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true; // เริ่มโหลด
    try {
      const res = await auth.login(username, password); // เรียกใช้ login จาก auth service
      localStorage.setItem("user", JSON.stringify(res.data.user)); // จัดเก็บข้อมูลผู้ใช้
      localStorage.setItem("token", res.data.access_token); // จัดเก็บ access token
      router.push("/"); // เปลี่ยนไปหน้าแรกหลังจากเข้าสู่ระบบสำเร็จ
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง"); // แสดงข้อความผิดพลาด
      console.log(e); // แสดงข้อผิดพลาดใน console
    } finally {
      loadingStore.isLoading = false; // หยุดโหลด
    }
  };

  const logout = () => {
    // authName.value = "";
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("name");
    localStorage.removeItem("email");
    router.replace("/login");
  };

  return { login, logout, isLogin, getUser };
});