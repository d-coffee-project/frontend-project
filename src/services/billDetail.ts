import type BillDetail from "@/types/BillDetail";
import http from "./axios";

function getBillDetail() {
  return http.get("/bill-detail");
}

function getBillDetailByID() {
  return http.get("/bill-detail/:id");
}

export default { getBillDetail, getBillDetailByID };
