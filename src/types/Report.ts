export default interface Report {
  id?: number;
  name: string;
  address: string;
  tel: string;
  email: string;
  position: string;
  wage: number;
  userId?: number;
  image?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
