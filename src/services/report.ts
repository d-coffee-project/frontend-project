import http from "./axios";
function getEmployee_info() {
  return http.get("/report/employee_view");
}

function getCustomer_info() {
  return http.get("/report/customer");
}

export default { getEmployee_info, getCustomer_info };
