import type Employee from "@/types/Employee";
import http from "./axios";
function getEmployee() {
  return http.get("/employee");
}

function getEmployee_info() {
  return http.get("/report/employee_view");
}

function saveEmployee(employee: Employee & { files: File[] }) {
  console.log("fileee:: ", employee.files);
  const formData = new FormData();
  formData.append("name", employee.name);
  formData.append("address", employee.address);
  formData.append("tel", employee.tel);
  formData.append("email", employee.email);
  formData.append("position", employee.position);
  formData.append("wage", `${employee.wage}`);
  formData.append("file", employee.files[0]);
  return http.post("/employee", formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function updateEmployee(id: number, employee: Employee & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", employee.name);
  formData.append("address", employee.address);
  formData.append("tel", employee.tel);
  formData.append("email", employee.email);
  formData.append("position", employee.position);
  formData.append("wage", `${employee.wage}`);
  if (employee.files) {
    formData.append("file", employee.files[0]);
  }

  return http.post(`/employee/${id}`, formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function deleteEmployee(id: number) {
  return http.delete(`/employee/${id}`);
}

function getEmployeeByName(name: string) {
  return http.get(`/employee/employee/${name}`);
}

export default {
  getEmployee,
  saveEmployee,
  updateEmployee,
  deleteEmployee,
  getEmployee_info,
  getEmployeeByName,
};
