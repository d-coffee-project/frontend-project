import LoginView from "@/views/LoginView.vue";
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
// function getUser() {
//   const user = localStorage.getItem("user");
// }

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Dashbord",
      components: {
        default: () => import("@/views/DashBoardVue.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/pos",
      name: "pos",

      components: {
        default: () => import("../views/PointOfSell/PointOfSellView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },

    {
      path: "/product",
      name: "product",
      components: {
        default: () => import("../views/products/ProductView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },

    {
      path: "/material",
      name: "material",
      components: {
        default: () => import("../views/Material/MaterialView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/checkMaterial",
      name: "Check Material",
      components: {
        default: () => import("../views/Material/CheckMaterialView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/bill",
      name: "Bill",
      components: {
        default: () => import("../views/bill/BillView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/employee",
      name: "employee",

      components: {
        default: () => import("../views/employees/EmployeeView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },

    {
      path: "/user",
      name: "user",

      components: {
        default: () => import("../views/users/UserView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },

    {
      path: "/customer",
      name: "customer",

      components: {
        default: () => import("../views/customers/CustomerView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },

    {
      path: "/store",
      name: "store",

      components: {
        default: () => import("../views/stores/StoreView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/cio",
      name: "cio",

      components: {
        default: () => import("../views/check_in_outs/CioView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/ciohistory",
      name: "ciohistory",

      components: {
        default: () => import("../views/check_in_outs/CioHistoryView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/salary",
      name: "salary",

      components: {
        default: () => import("../views/Salary/SalaryView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/salaryhistory",
      name: "salaryhistory",

      components: {
        default: () => import("../views/Salary/SalaryHistoryView.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/history",
      name: "history",

      components: {
        default: () => import("../views/PointOfSell/RecieptHistory.vue"),
        menu: () => import("@/components/Menu/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
  ],
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return false;
  }
  return false;
}
router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
