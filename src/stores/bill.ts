import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Bill from "@/types/Bill";
import type BillDetail from "@/types/BillDetail";
import billService from "@/services/bill";
import billDetail from "@/services/billDetail";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import BillDialogVue from "@/views/bill/BillDialog.vue";

export const useBillStore = defineStore("Bill", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  // const authStore = useAuthStore();
  const Bill = ref<Bill[]>([]);

  const BillDetail = ref<
    {
      materialId: number;
      name: string;
      amount: number;
      price: number;
      total: number;
    }[]
  >([]);
  const navDraw = ref(false);
  const dialog = ref(false);
  const editedBill = ref<{
    materialId: number;
    name: string;
    amount: number;
    price: number;
    total: number;
  }>({ materialId: 0, name: "", amount: 0, price: 0, total: 0 });

  const temp = ref();
  const billHeader = ref<Bill>({
    shop_name: "",
    total: 0,
    buy: 0,
    change: 0,
    employeeId: 0,
    billDetail: BillDetail.value,
  });

  watch(temp, (newTemp) => {
    if (newTemp) {
      editedBill.value = {
        materialId: temp.value.mat_id,
        name: temp.value.mat_name,
        amount: 0,
        price: 0,
        total: 0,
      };
    }
  });

  // watch(temp, (newTemp) => {
  //   if (newTemp) {
  //     billHeader.value = {
  //       shop_name: "",
  //       total: 0,
  //       buy: 0,
  //       change: 0,
  //       employeeId: 0,
  //       billDetail: BillDetail.value,
  //     };
  //   }
  // });

  async function getBills() {
    // loadingStore.isLoading = true;
    try {
      const res = await billService.getBill();
      Bill.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }

  async function addBill() {
    // loadingStore.isLoading = true;

    const index = BillDetail.value.findIndex(
      (item) => item.materialId == editedBill.value.materialId
    );
    if (index == -1) {
      BillDetail.value.push(editedBill.value);
    } else {
      BillDetail.value[index].amount = editedBill.value.amount;
      BillDetail.value[index].price = editedBill.value.price;
      BillDetail.value[index].total = editedBill.value.total;
    }
    editedBill.value = {
      materialId: 0,
      name: "",
      amount: 0,
      price: 0,
      total: 0,
    };
    temp.value = null;
    navDraw.value = false;
    loadingStore.isLoading = false;
  }

  async function saveBill() {
    loadingStore.isLoading = true;

    try {
      const res = await billService.saveBill(billHeader.value);
    } catch (error) {
      console.log(error);
    }
  }

  async function editBill() {}

  function deleteBillDetail(id: number): void {
    const index = BillDetail.value.findIndex((item) => item.materialId == id);
    if (index !== -1) {
      BillDetail.value.splice(index, 1);
    }
  }

  async function deleteBill(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await billService.deleteBill(id);
      await getBills();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Bill ได้");
    }
    // loadingStore.isLoading = false;
  }

  return {
    Bill,
    getBills,
    addBill,
    deleteBill,
    navDraw,
    saveBill,
    editBill,
    editedBill,
    dialog,
    BillDetail,
    temp,
    billHeader,
    deleteBillDetail,
  };
});
