import type CheckMaterial from "./CheckMaterial";
import type Employee from "./Employee";
import type Material from "./Material";

export default interface CheckMaterialDetail {
  cmd_id?: number;
  cmd_name?: string;
  cmd_qty_last?: number;
  cmd_qty_remain?: number;
  cmd_qty_expire?: number;
  check_material?: CheckMaterial;
  matId?: number;
  material?: Material;
  deletedAt?: Date;
}
