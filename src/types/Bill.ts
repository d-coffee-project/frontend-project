import type BillDetail from "./BillDetail";
import type Employee from "./Employee";

export default interface Bill {
  id?: number;
  shop_name?: string;

  date?: String;

  time?: String;

  total?: number;

  buy?: number;

  change?: number;

  employeeId?: number;

  billDetail?: BillDetail[];

  createDate?: Date;
  updateDate?: Date;
  deletedDate?: Date;
}
