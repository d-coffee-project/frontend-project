import type Material from "@/types/Material";
import { defineStore } from "pinia";
import { ref, watch } from "vue";
import materialService from "@/services/material";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useMaterialStore = defineStore("Material", () => {
  const navDraw = ref(false);
  const materials = ref<Material[]>([]);
  const editMaterial = ref<Material>({
    mat_name: "",
    mat_min_quantity: 0,
    mat_quantity: 0,
    mat_unit: "",
    mat_price_per_unit: 0,
  });

  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();

  //watch เมื่อค่า dialog มีการเปลี่ยนแปลง และเก็บค่าลงใน editMaterial
  watch(navDraw, (newNav, oldNav) => {
    if (!newNav) {
      editMaterial.value = {
        mat_name: "",
        mat_min_quantity: 0,
        mat_quantity: 0,
        mat_unit: "",
        mat_price_per_unit: 0,
      };
    }
  });

  //ดึงของที่อยู่ใน servers
  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getMaterials();
      materials.value = res.data[0];
      console.log(materials);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveMaterial() {
    loadingStore.isLoading = true;
    //save เข้า db ใหม่
    try {
      if (editMaterial.value.mat_id) {
        const res = await materialService.updateMaterial(
          editMaterial.value.mat_id,
          editMaterial.value
        );
      } else {
        const res = await materialService.saveMaterial(editMaterial.value);
      }
      navDraw.value = false;
      await getMaterials(); //เพื่อให้updateตัวเอง
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกได้");
    }
    loadingStore.isLoading = false;
  }
  async function deleteMaterial(mat_id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.deleteMaterial(mat_id);
      await getMaterials(); //เพื่อให้updateตัวเอง
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบได้");
    }
    loadingStore.isLoading = false;
    messageStore.showSuccess("ลบข้อมูลเสร็จสิ้น");
  }

  function fixMaterial(material: Material) {
    //edit ที่มีอยู่แล้ว ที่btnEdit
    editMaterial.value = material;
    navDraw.value = true;
  }

  async function searcMaterial(event: Event) {
    const { target } = event;
    const key = (target as HTMLButtonElement).value;
    // loadingStore.isLoading = true;
    try {
      let data = [];
      if (key) {
        const res = await materialService.getMaterialsByName(key);
        data = res.data;
      } else {
        const res = await materialService.getMaterials();
        data = res.data;
      }
      materials.value = data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    // loadingStore.isLoading = false;
  }

  return {
    materials,
    getMaterials,
    navDraw,
    saveMaterial,
    editMaterial,
    fixMaterial,
    deleteMaterial,
    searcMaterial,
  };
});
