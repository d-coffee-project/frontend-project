import type CheckInOut from "./CheckInOut";

export default interface Salary {
  id?: number;

  date?: string;

  workhour?: number;

  salary?: number;

  status?: string;

  checkinout?: CheckInOut;

  createdDate?: Date;

  updatedDate?: Date;

  deletedDate?: Date;
}
