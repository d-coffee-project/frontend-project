import type CheckMaterial from "@/types/CheckMaterial";
import http from "./axios";
function getCheckMaterial() {
  return http.get("/check-mat");
}

function getcheckMaterialById(cmd_id: number) {
  return http.get(`/check-mat/${cmd_id}`);
}
function savecheckMaterial(CheckMaterial: CheckMaterial) {
  return http.post("/check-mat", CheckMaterial);
}

function updateCheckMaterial(cmd_id: number, CheckMaterial: CheckMaterial) {
  return http.patch(`/check-mat/${cmd_id}`, CheckMaterial);
}

function deleteCheckMaterial(cmd_id: number) {
  return http.delete(`/check-mat/${cmd_id}`);
}

export default {
  getCheckMaterial,
  getcheckMaterialById,
  savecheckMaterial,
  updateCheckMaterial,
  deleteCheckMaterial,
};
