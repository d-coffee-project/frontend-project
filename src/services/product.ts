import type Product from "@/types/Product";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}

function getProductsByCategory(category: number) {
  return http.get(`/products/category/${category}`);
}

function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("price", `${product.price}`);
  formData.append("file", product.files[0]); //ดูจากpostman
  formData.append("categoryId", `${product.categoryId}`);
  return http.post("/products", formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
}

function updateProduct(id: number, product: Product) {
  return http.patch(`/products/${id}`, product);
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

function getProductsByCategoryAndName(category: number, name: string) {
  return http.get(`/products/product/${category}/${name}`);
}

export default {
  getProducts,
  saveProduct,
  updateProduct,
  deleteProduct,
  getProductsByCategory,
  getProductsByCategoryAndName,
};
