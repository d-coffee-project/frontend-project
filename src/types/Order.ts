import type Customer from "./Customer";
import type OrderItem from "./OrderItem";

export default interface Order {
  id?: number;
  amount?: number;
  total?: number;
  recevied?: number;
  // discount?: number;
  change?: number;
  payment?: string;
  customer?: Customer;

  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;

  orderItems?: OrderItem[];
  userId?: number;
  storeId?: number;
}
