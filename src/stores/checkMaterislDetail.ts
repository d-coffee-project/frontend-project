import type Material from "@/types/Material";
import { defineStore } from "pinia";
import { computed, ref, watch, type Ref } from "vue";
import { useMaterialStore } from "./material";
import type CheckMaterialDetail from "@/types/CheckMaterialDetail";
import checkMaterialService from "@/services/checkMaterial";
import type CheckMaterial from "@/types/CheckMaterial";
import employee from "@/services/employee";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useCheckMaterialStore = defineStore("checkMaterial", () => {
  const navDrawCheckMat = ref(false);
  const dialog = ref(false);
  const dialogShowMatDetail = ref(false);
  const checkMat = ref<CheckMaterial[]>([]); // เก็บข้อมูล CheckMaterial ที่ดึงมาจาก API
  const temp = ref();
  const matD = ref<
    //เก็บรายการ CheckMaterialDetail ที่ถูกเพิ่มเข้าไปในรายการตรวจสอบ
    {
      matId: number;
      cmd_name: string;
      mat_qty_last: number;
      mat_qty_remain: number;
      mat_qty_expire: number;
    }[]
  >([]);

  const editCheckMaterial = ref<{
    // เก็บข้อมูล CheckMaterial ที่ถูกแก้ไข
    matId: number;
    cmd_name: string;
    mat_qty_last: number;
    mat_qty_remain: number;
    mat_qty_expire: number;
  }>({
    matId: 0,
    cmd_name: "",
    mat_qty_last: 0,
    mat_qty_remain: 0,
    mat_qty_expire: 0,
  });

  //watch เมื่อค่า dialog มีการเปลี่ยนแปลง และเก็บค่าลงใน editMaterial
  watch(dialog, (newDialog, oldNav) => {
    if (!newDialog) {
      matD.value = [];
    }
  });

  watch(temp, (newTemp) => {
    if (newTemp) {
      editCheckMaterial.value = {
        matId: temp.value.mat_id,
        cmd_name: temp.value.mat_name,
        mat_qty_last: temp.value.mat_quantity,
        mat_qty_expire: 0,
        mat_qty_remain: 0,
      };
    }
  });

  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();

  // ดึงข้อมูล CheckMaterial จาก API
  async function getCheckMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await checkMaterialService.getCheckMaterial();
      checkMat.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
    dialog.value = false;
    loadingStore.isLoading = false;
  }

  const checkM = ref<{ check_mat_date: string; check_mat_time: string }>();
  const checkMatD = ref<
    {
      cmd_id: number;
      cmd_name: string;
      cmd_qty_last: number;
      cmd_qty_remain: number;
      cmd_qty_expire: number;
    }[]
  >([]);

  async function getcheckMaterialById(cmd_id: number) {
    navDrawCheckMat.value = false;
    loadingStore.isLoading = true;
    try {
      const res = await checkMaterialService.getcheckMaterialById(cmd_id);
      checkM.value = res.data;
      checkMatD.value = res.data.check_material_detail;
      dialogShowMatDetail.value = true;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  //เพิ่มข้อมูล CheckMaterialDetail เข้าไปใน matD
  const addCheckMatDetails = () => {
    loadingStore.isLoading = true;
    const index = matD.value.findIndex(
      (item) => item.matId == editCheckMaterial.value.matId
    );

    if (index == -1) {
      matD.value = [...matD.value, editCheckMaterial.value];
    } else {
      matD.value[index] = {
        ...matD.value[index],
        mat_qty_expire: editCheckMaterial.value.mat_qty_expire,
        mat_qty_last: editCheckMaterial.value.mat_qty_last,
        mat_qty_remain: editCheckMaterial.value.mat_qty_remain,
      };
    }
    navDrawCheckMat.value = false;
    loadingStore.isLoading = false;
  };

  function deleteCheckMatDetail(mat_id: number): void {
    const index = matD.value.findIndex((item) => item.matId == mat_id);
    if (index !== -1) {
      matD.value.splice(index, 1);
    }
  }
  async function deleteCheckMat(mat_id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkMaterialService.deleteCheckMaterial(mat_id);
      await getCheckMaterials();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    loadingStore.isLoading = false;
    messageStore.showSuccess("ลบข้อมูลเสร็จสิ้นแล้ว");
  }

  function editCheckMatDetail(item: {
    matId: number;
    name: string;
    cmd_qty_last: number;
    cmd_qty_remain: number;
    cmd_qty_expire: number;
  }) {
    editCheckMaterial.value = {
      matId: item.matId,
      cmd_name: item.name,
      mat_qty_last: item.cmd_qty_last,
      mat_qty_remain: item.cmd_qty_remain,
      mat_qty_expire: item.cmd_qty_expire,
    };
  }

  const empId = parseInt(localStorage.getItem("empId")!);
  async function savecheckMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await checkMaterialService.savecheckMaterial({
        empId: empId,
        check_material_detail: matD.value,
        check_mat_date: "",
        check_mat_time: "",
      });
      await getCheckMaterials();
    } catch (e) {
      console.log(e);
    }
    dialog.value = false;
    loadingStore.isLoading = true;
  }
  loadingStore.isLoading = false;

  return {
    getCheckMaterials,
    editCheckMatDetail,
    savecheckMaterial,
    deleteCheckMatDetail,
    addCheckMatDetails,
    deleteCheckMat,
    navDrawCheckMat,
    editCheckMaterial,
    dialog,
    checkMat,
    matD,
    temp,
    dialogShowMatDetail,
    checkMatD,
    getcheckMaterialById,
  };
});
