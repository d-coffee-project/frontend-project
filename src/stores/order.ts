import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type OrderItem from "@/types/OrderItem";
import type Product from "@/types/Product";
import type Order from "@/types/Order";
import OrderService from "@/services/order";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useAuthStore } from "./auth";
import { useCustomerStore } from "@/stores/customer";
import { useProductStore } from "./product";
export const useOrderStore = defineStore("Order", () => {
  const productStore = useProductStore();
  const order = ref<Order[]>([]);
  const OrderItem = ref<OrderItem[]>([]);
  const OrderView = ref<OrderItem[]>([]);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const dialog = ref(false);
  const member = ref();
  const model = ref(false);
  const CASH = ref(0);
  const tel = ref("");
  const payment = ref("Cash");
  const dialogInvoice = ref(false);
  const CustomerStore = useCustomerStore();
  const orderList = ref<
    {
      product: Product;
      amount: number;
      sum: number;
      receive: number;
      change: number;
    }[]
  >([]);

  // const getUser = () => {
  //   const userString = localStorage.getItem("user");
  //   if (!userString) return null;
  //   const user = JSON.parse(userString ?? "");
  //   console.log(user);
  // };

  //คำนวนเงินทอน
  const change = computed(() => {
    return Math.max(CASH.value - sumPrice.value, 0);
  });

  //คำนวนภาษี
  // const calVAT = computed(() => {
  //   return (sumPrice.value * 7) / 100;
  // });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      model.value = false;
    }
  });

  function clearOrder() {
    orderList.value = [];
    OrderItem.value.splice(0, OrderItem.value.length);
    OrderView.value.splice(0, OrderView.value.length);
    CASH.value = 0;
    tel.value = "";
  }

  function addNewOrderItem(item: Product) {
    const index = OrderItem.value.findIndex(
      (orderItems) => orderItems.productId === item.id
    );
    console.log(index);

    if (index === -1) {
      const newOrderItem = { productId: item.id!, amount: 1 };
      OrderItem.value.push(newOrderItem);

      const newOrderView = {
        productId: item.id!,
        product: item,
        price: item.price,
        amount: 1,
        total: item.price,
      };
      productStore.dialogAdd = false;
      OrderView.value.push(newOrderView);
    } else {
      OrderItem.value[index].amount++;
      OrderView.value[index].amount++;
      OrderView.value[index].total =
        OrderView.value[index].amount * OrderView.value[index].price!;
    }
  }

  function RemoveOrderItem(item: OrderItem) {
    const index = OrderItem.value.findIndex(
      (orderItems) => orderItems.productId === item.productId
    );
    // console.log(index);
    if (OrderItem.value[index].amount > 1) {
      OrderItem.value[index].amount--;
      OrderView.value[index].amount--;
      OrderView.value[index].total =
        OrderView.value[index].amount * OrderView.value[index].price!;
    } else {
      btnRemoveOrderItem(item);
    }
  }

  function btnRemoveOrderItem(item: OrderItem) {
    const index = OrderItem.value.findIndex(
      (orderItems) => orderItems.productId === item.productId
    );
    OrderItem.value.splice(index, 1);
    OrderView.value.splice(index, 1);
  }

  //คิดจำนวนของ(ชิ้น)
  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < OrderItem.value.length; i++) {
      sum = sum + OrderItem.value[i].amount;
    }
    return sum;
  });

  //คิดราคารวมของ(เงินรวม)
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < OrderView.value.length; i++) {
      const item = OrderView.value[i];
      sum += item.amount * item.price!;
    }
    return sum;
  });

  const orderBill = ref<{
    id: number;
    createdDate: string;
    time: string;
    total: number;
    recevied: number;
    change: number;
  }>();
  const orderDetail = ref<{ amount: number; name: string; price: number }[]>(
    []
  );

  async function openOrder() {
    loadingStore.isLoading = true;
    const user: { id: number } = authStore.getUser();
    const orderItems = OrderView.value.map(
      (item) =>
        <{ productId: number; amount: number }>{
          productId: item.productId,
          amount: item.amount,
        }
    );

    const order = {
      userId: user.id,
      orderItems: orderItems,
      recevied: CASH.value,
      payment: payment.value,
    };
    console.log(order);
    try {
      const res = await OrderService.saveOrder(order);
      orderBill.value = res.data;
      orderDetail.value = res.data.orderItems;
      messageStore.showSuccess("บันทึก Order สำเร็จ!");
      dialogInvoice.value = true;
      clearOrder();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getOrder() {
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.getOrder();
      order.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getOrderID(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.getOrderID(id);
      orderBill.value = res.data;
      orderDetail.value = res.data.orderItems;
      console.log(orderBill.value);
      console.log(orderDetail.value);
      dialogInvoice.value = true;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    OrderItem,
    addNewOrderItem,
    btnRemoveOrderItem,
    RemoveOrderItem,
    OrderView,
    dialog,
    member,
    model,
    sumPrice,
    sumAmount,
    orderList,
    openOrder,
    clearOrder,
    change,
    CASH,
    payment,
    getOrder,
    tel,
    order,
    orderDetail,
    orderBill,
    dialogInvoice,
    getOrderID,
  };
});
