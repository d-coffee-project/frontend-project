import type CheckInOut from "../types/CheckInOut";
import http from "./axios";
function getCio() {
  return http.get("/check-in-out");
}

function getCioByEmail(email: string) {
  return http.get(`/check-in-out/${email}`);
}

function saveCio(Cio: CheckInOut) {
  console.log(Cio);
  return http.post("/check-in-out", Cio);
}

function updateCio(Cio: CheckInOut) {
  return http.patch("/check-in-out", Cio);
}

function check(email: string) {
  return http.get(`/check-in-out/check/${email}`);
}
export default { getCio, saveCio, updateCio, check, getCioByEmail };
